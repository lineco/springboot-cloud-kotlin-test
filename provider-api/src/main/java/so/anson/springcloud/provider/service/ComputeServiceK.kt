package so.anson.springcloud.provider.service

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam

/**
 * Created by wangmeng on 2017/6/14.
 */
interface ComputeServiceK{
    @RequestMapping(value = "/add", method = arrayOf(RequestMethod.GET))
    fun add(@RequestParam("a") a:Int, @RequestParam("b") b:Int):Int;
}
