package so.anson.springcloud.proxy

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties
import org.springframework.cloud.netflix.zuul.filters.discovery.DiscoveryClientRouteLocator
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

/**
 * Created by wangmeng on 2017/6/14.
 */
@Component
class ProxyIniterK{
    @Autowired
    lateinit var routeLocator:DiscoveryClientRouteLocator

    @PostConstruct
    fun init(){
        val dynamicAddRoute = ZuulProperties.ZuulRoute()
        dynamicAddRoute.path = "/new-add-route/**"
        dynamicAddRoute.serviceId = "compute-service"
        routeLocator.addRoute(dynamicAddRoute)
    }
}
