package so.anson.springcloud.proxy

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.zuul.EnableZuulProxy

/**
 * Created by wangmeng on 2017/6/14.
 */
@EnableZuulProxy
@SpringBootApplication
open class ProxyApplicationK

fun main(args:Array<String>){
    SpringApplication.run(ProxyApplicationK::class.java,*args)
}