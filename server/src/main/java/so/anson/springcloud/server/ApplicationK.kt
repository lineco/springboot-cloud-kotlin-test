package so.anson.springcloud.server

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

/**
 * Created by wangmeng on 2017/6/14.
 */
@EnableEurekaServer
@SpringBootApplication
class ApplicationK

fun main(args:Array<String>){
    SpringApplication.run(ApplicationK::class.java,*args)
}