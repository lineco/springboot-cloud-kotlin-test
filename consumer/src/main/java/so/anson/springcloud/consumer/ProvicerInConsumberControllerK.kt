package so.anson.springcloud.consumer

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

/**
 * Created by wangmeng on 2017/6/14.
 */
@RestController
class ProvicerInConsumberControllerK{
    @RequestMapping(value = "/minus", method = arrayOf(RequestMethod.GET))
    fun minus(@RequestParam a: Int, @RequestParam b: Int): Int {
        return a - b
    }
}
