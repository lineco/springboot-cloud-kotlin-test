package so.anson.springcloud.consumer

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.client.loadbalancer.LoadBalanced
import org.springframework.cloud.netflix.feign.EnableFeignClients
import org.springframework.context.annotation.Bean
import org.springframework.web.client.RestTemplate

/**
 * Created by wangmeng on 2017/6/14.
 */

@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
open class ConsumerApplicationK{
    @Bean
    @LoadBalanced
    open fun restTemplate(): RestTemplate{
        return RestTemplate()
    }
}
fun main(args:Array<String>){
    SpringApplication.run(ConsumerApplicationK::class.java,*args)
}