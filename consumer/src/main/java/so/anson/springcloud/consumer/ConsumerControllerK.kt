package so.anson.springcloud.consumer

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate

/**
 * Created by wangmeng on 2017/6/14.
 */
@RestController
class ConsumerControllerK{
    @Autowired
    lateinit var computeClient:ComputeClientK

    @Autowired
    lateinit var restTemplate: RestTemplate

    @RequestMapping(value = "/add",method = arrayOf(RequestMethod.GET))
    fun add():Int{
        val result = computeClient.add(10, 20)
        println("service call result: " + result)
        return result;
    }

    @RequestMapping(value = "/add2", method = arrayOf(RequestMethod.GET))
    fun add2(): Int? {
        val result = restTemplate.getForEntity<Int>("http://compute-service/add?a=1&b=2", Int::class.java).body
        println("service call result: " + result!!)
        return result
    }
}

