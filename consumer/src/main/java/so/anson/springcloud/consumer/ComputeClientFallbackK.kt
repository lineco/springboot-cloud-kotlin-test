package so.anson.springcloud.consumer

import org.springframework.stereotype.Service

/**
 * Created by wangmeng on 2017/6/14.
 */
@Service
class ComputeClientFallbackK : ComputeClientK {
    override fun add(a: Int, b: Int): Int {
        return -99
    }
}

