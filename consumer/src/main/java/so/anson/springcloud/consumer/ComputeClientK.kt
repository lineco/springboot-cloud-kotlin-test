package so.anson.springcloud.consumer

import org.springframework.cloud.netflix.feign.FeignClient
import so.anson.springcloud.provider.service.ComputeServiceK

/**
 * Created by wangmeng on 2017/6/14.
 */
@FeignClient(value = "compute-service",fallback = ComputeClientFallbackK::class)
interface ComputeClientK : ComputeServiceK