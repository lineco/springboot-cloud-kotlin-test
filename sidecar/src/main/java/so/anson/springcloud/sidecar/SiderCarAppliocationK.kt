package so.anson.springcloud.sidecar

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.sidecar.EnableSidecar

/**
 * Created by wangmeng on 2017/6/14.
 */
@EnableSidecar
@SpringBootApplication
open class SiderCarAppliocationK

fun main(args: Array<String>){
    SpringApplication.run(SiderCarAppliocationK::class.java,*args);
}
