package so.anson.springcloud.provider

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.client.discovery.DiscoveryClient
import org.springframework.web.bind.annotation.RestController
import so.anson.springcloud.provider.service.ComputeServiceK

/**
 * Created by wangmeng on 2017/6/14.
 */
@RestController
open class ComputeControllerK : ComputeServiceK {

    @Autowired
    lateinit open var client: DiscoveryClient

    override fun add(a: Int, b: Int): Int
    {
        var result=a+b
        val localServiceInstance = client.localServiceInstance
        println("/add invoked, service: " + localServiceInstance)
        println("result: " + result)
        return result;

    }
}

