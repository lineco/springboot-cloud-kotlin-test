package so.anson.springcloud.provider

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

/**
 * Created by wangmeng on 2017/6/14.
 */
@EnableDiscoveryClient
@SpringBootApplication
open class ProviderApplicationK

fun main(args: Array<String>){
    SpringApplication.run(ProviderApplicationK::class.java,*args)
}
